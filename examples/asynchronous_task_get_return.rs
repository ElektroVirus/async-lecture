#[tokio::main]
async fn main() {
    let url = "http://wapice-rust-cert.northeurope.cloudapp.azure.com:8000/get-certificates";

    let request_join_handle = tokio::spawn(async move {
        let result = send_request(url).await;
        if let Some(res) = result {
            println!("{}", res);
        }
    });

    println!("Hello, world!");

    if let Err(e) = request_join_handle.await {
        println!("{}", e);
    }

}

async fn send_request(url: &str) -> Option<String> {
    if let Ok(req) = reqwest::get(url).await {
        if let Ok(body) = req.text().await {
            return Some(body);
        }
    }
    None
}



// cargo init async-lecture
// cargo add reqwest --features=blocking
// cargo add tokio --features=full
