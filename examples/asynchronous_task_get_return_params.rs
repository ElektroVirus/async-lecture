use std::sync::{Arc, Mutex, MutexGuard};

#[tokio::main]
async fn main() {
    let url = "http://wapice-rust-cert.northeurope.cloudapp.azure.com:8000/get-certificates";

    let counter = Arc::new(Mutex::new(0.00f32));
    let counter_clone = counter.clone();

    let request_join_handle = tokio::spawn(async move {
        let result = send_request(url, counter_clone).await;
        if let Some(res) = result {
            println!("{}", res);
        }
    });

    println!("Hello, world!");

    if let Err(e) = request_join_handle.await {
        println!("{}", e);
    }

    let locked_counter: MutexGuard<f32> = counter.lock().unwrap();
    println!("{}", locked_counter);
}

async fn send_request(url: &str, counter: Arc<Mutex<f32>>) -> Option<String> {
    {
        let mut locked_counter: MutexGuard<f32> = counter.lock().unwrap();
        *locked_counter += 1.0;
    }

    if let Ok(req) = reqwest::get(url).await {
        if let Ok(body) = req.text().await {
            return Some(body);
        }
    }
    None
}
