#[tokio::main]
async fn main() {
    let url = "http://wapice-rust-cert.northeurope.cloudapp.azure.com:8000/get-certificates";

    send_request(url).await;

    println!("Hello, world!");
}

async fn send_request(url: &str) {
    if let Ok(req) = reqwest::get(url).await {
        if let Ok(body) = req.text().await {
            println!("{}", body);
        }
    }
}



// cargo init async-lecture
// cargo add reqwest --features=blocking
// cargo add tokio --features=full
