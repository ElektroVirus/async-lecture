fn main() {
    let url = "http://wapice-rust-cert.northeurope.cloudapp.azure.com:8000/get-certificates";

    send_request(url);

    println!("Hello, world!");
}

fn send_request(url: &str) {
    if let Ok(req) = reqwest::blocking::get(url) {
        if let Ok(body) = req.text() {
            println!("{}", body);
        }
    }
}



// cargo init async-lecture
// cargo add reqwest --features=blocking
// cargo add tokio --features=full
