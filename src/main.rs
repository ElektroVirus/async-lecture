#[tokio::main]
async fn main() {
    let url = "http://wapice-rust-cert.northeurope.cloudapp.azure.com:8000/get-certificates";

    println!("{}", send_request(url).await);

    println!("Hello, world!");
}

async fn send_request(url: &str) -> String {
    let mut response: String = String::new();
    if let Ok(req) = reqwest::get(url).await {
        if let Ok(body) = req.text().await {
            response = body
        }
    }
    response
}



// cargo init async-lecture
// cargo add reqwest --features=blocking
// cargo add tokio --features=full
